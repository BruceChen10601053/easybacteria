/* 細菌達人程式碼
陳光穎 Bruce Chen
2021/8/14
版本: 1.0.0.0
限制: 最多1000個藥名，每個藥名最長1000字元 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/* Global variable */
char DrugBuffer[1000000]; // 神內精神麻醉藥物.txt 的暫存
char WordBank[1000][1001]; // 藥名 word bank
int N; // number of drugs

/* 程式進入點 */
int main(){
    printf("細菌達人\r\n本程式隨機輸出一個細菌、細菌引起的疾病或免疫。\r\n");
    FILE* A = fopen("細菌達人資料庫.txt", "r");
    if(A == NULL){
        printf("File not found!\n");
    }
    int i;
    for(i=0; i<1000; i++){
        char* f = fgets(WordBank[i], 1000, A);

        if(f == NULL){
            break;
        }
        if(strlen(WordBank[i]) > 700){
            printf("警告: 有藥物名稱過長。請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n");
        }
    }
    N = i;
    printf("資料庫有%d個\r\n", N);
    if(N >= 50000){
        printf("警告: 資料庫藥物過多。請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n");
    }
    srand(time(NULL)); // 設定亂數種子
    system("pause");
    while(1){
        int r = rand() % N; // 產生 0~N 的亂數
        printf(WordBank[r]);
        system("pause");
    }
    return 0;
}

